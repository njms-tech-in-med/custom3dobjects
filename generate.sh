#!/bin/bash
set -e #if anything goes wrong, abort the whole thing
mkdir -p generated
total=$(wc -l < names.txt)
count=1
RED='\033[0;32m'
NC='\033[0m' # No Color
echo -e "${RED} Generating $total STL files from names.txt x${NC}"
while read p; do
	readarray -d "," -t line <<< $p
	name=${line[0]}
	large=${line[1]}
	large=$(echo "${large}" | sed -e 's/^[ \t]*//')
	echo $name
	openscad/openscad.exe -o "tags/$p.stl" -D "name=\"$name\"" -D "large=\"$large\"" tag.scad
	echo "Completed $count of $total..."
	count=`expr $count + 1`
done <names.txt

echo -e "${RED}All STL files generated! Now, to slice!${NC}"

/c/Program\ Files/Prusa3D/Slic3rPE/slic3r.exe -m --load config.ini --gui tags/*

echo -e "${RED}Cleaning up...${NC}"
#no longer generating a gcode file, just opening slic3r GUI, since human intervention is needed for printing anyway
#mv tags/tags.gcode .
rm -rf tags/

echo -e "${RED}Slicing complete! Upload tags.gcode to printer, confirm it looks good, and print!${NC}"
