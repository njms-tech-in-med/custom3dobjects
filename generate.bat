@echo off
setlocal EnableDelayedExpansion
rem This was a whole process. Makes me miss bash. In case you haven't noticed, commends in batch files are preceded by 'rem' for some reason --Toolbox

rem change to the directory that the batch file is in so we don't accidentally get run from somewhere else and mess stuff up wherever that is
cd /D "%~dp0" 

rem self explanatory
if exist generated rmdir /S /Q generated
mkdir generated

echo ================================================================
echo ============= Generating STL files from names.txt ==============
echo ================================================================

rem this loop translates roughly to: for each line (skipping the first one), find everything separated by commas/spaces and put it in up to 10 tokens from %%a to %%j. Yeah It's basically magic. I don't get it either.
for /f "skip=1 usebackq tokens=1-10 delims=," %%a in ("names.txt") do (
    setlocal EnableDelayedExpansion
    rem name should be in %%a, and size should be in %%b
    set name=%%a
    set size=%%b
    
    rem we apparently start using exclamation marks to evaluate variables when we enter for loops for reasons I don't want to explain.
    IF [!size!] == [] set size=small
    echo Generating "!name!" in size !size!
    openscad\openscad.exe -o "generated/!name!.stl" -D "name=\"!name!\"" -D "large=\"!size!\"" tag.scad

)

rem bend over backwards just to get a list of files in a directory. store them to files
set files=
for /f "tokens=* USEBACKQ" %%K IN (`dir /b generated\*`) DO ( SET files=!files! "generated\%%K" )

echo "========= All STL files generated! Now, to slice! ========="

rem open prusaslicer with our files!
"C:\Program Files\Prusa3D\PrusaSlicer\prusa-slicer-console.exe" --repair %files%
