# Tech-in-Med Custom 3D Models

This is where Tech-in-Med stores its custom-designed 3D models for printing and information. These may be used freely, and many are sold as a part of our fundraisers. The files stored here are open source and licensed under GPL 

## Generating models

The steps for generating models to be printed is very simple!

0. Make sure [PrusaSlicer](https://www.prusa3d.com/prusaslicer/) is installed first
1. Fill out names.txt. It's pretty straightforward right now ("name, size" for each line), but could expand in the future. An example line is included in the file.
2. Run generate.bat
3. If you have PrusaSlicer properly installed, it should pop up with all of your pretty tags! Just hit arrange, adjust settings, slice, and print as usual!

## Licensing

The contents of this repository (unless otherwise stated, see below) is licensed by the MIT license.

### Included software

Included wholesale in its original compiled form is OpenSCAD, a wonderful program used to design and generate models from parametric programs. This program is very useful and is leveraged as an external dependency of our work. It is included for convenience and is licensed under the GPLv2.0 License. It is Copyright (C) 2009-2019 The OpenSCAD Developers. 