//Copyright 2020 Jason 'Toolbox' Oettinger, all rights waived.
//measurements in millimeters
name="NJMS";
large="false";
height=(large=="large" || large==" large" ? 30 : 20);
StethoscopeTag(name, height=height);
// Module definition.

//module StethescopeTag(text, emboss) {
//    d=emboss?-2:2;
//    Name(text, depth=d, height=30, textdepth=d, rounding=2);
//}


module StethoscopeTag(text, emboss=false, depth=4, height=30, rounding=3) {
    width=len(text)*15*height/30;
    depth = depth * height/30;
    depthoffset=depth;
    textdepth=depth*0.7;
    if (emboss) {
        difference() {
            translate([0,0,depth/2]) RoundedRectangle(depth,height,width,rounding);
            SizedText(text,width,height,depthoffset-textdepth, textdepth);
        };
    } else {
        union() {
            translate([-width/2,0,0]) scale([height/30,height/30,1]) resize([0,0,depth]) TagPiece();
            
                translate([0,0,depth/2]) RoundedRectangle(depth,height,width,rounding);
            SizedText(text,width,height,0, depth + textdepth);
            
        }
    }
}

module TagPiece() {
    depth=7;
    translate([0,0,depth/2])
        difference() {
            RoundedRectangle(depth=depth,width=15,height=20,rounding=2);
            translate([-2.5,0,0]) RoundedRectangle(depth=depth,width=4,height=13,rounding=1);
            translate([0.5,-15,-15]) cube(30);
        }
}

module SizedText(text, width, height, depthoffset, depth) {
    translate([0,0,depthoffset])
            resize([width-5,height*0.7,depth])
            linear_extrude(height=15, convexity=10)
            text(text,font="Liberation Mono", halign="center", valign="center", $fn=60);
}

module RoundedRectangle(depth, height, width, rounding) {
    minkowski() {
            cube([width-(2*rounding),height-(2*rounding),depth], center=true);
            cylinder(r=rounding,h=0.001, $fn=90);
    }
}